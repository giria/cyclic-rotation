//
//  AppDelegate.m
//  CyclicRotation
//
//  Created by Joan Barrull on 16/02/2020.
//  Copyright © 2020 Joan Barrull. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    NSLog(@"hello world");
    
    NSMutableArray * a = @[ @3, @8, @9, @7, @6].mutableCopy;
    [self solutionWithArray:a andOffset:3];
    return YES;
}



- (NSMutableArray *) solutionWithArray:(NSMutableArray *)A andOffset: (int) K {
    
    NSMutableArray *rotatedA = [NSMutableArray arrayWithCapacity:A.count];
    for(int i=0; i<A.count;i++) {
        rotatedA[i] = @(0);
    }
  
       
       for(int i=0; i<A.count; i++) {
         //rotated index needs to "wrap" around end of array
         int rotatedIndex = (i + K) % A.count;

         rotatedA[rotatedIndex] = A[i];
       }
       return rotatedA;
    
}


#pragma mark - UISceneSession lifecycle





@end
