//
//  AppDelegate.h
//  CyclicRotation
//
//  Created by Joan Barrull on 16/02/2020.
//  Copyright © 2020 Joan Barrull. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@end

